export const Colors: any = {
  Primary: '#E91E63',
  Secondary: '#9C0A3B',
};

export const Size: any = {
  XLMax: '32px',
  Max: '24px',
  Normal: '16px',
  Min: '14px',
};
