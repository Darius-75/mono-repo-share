import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom/client';

import App from './app/app';
import styled from 'styled-components';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
const Global = styled.div`
  box-sizing: border-box;
  padding: 0;
  margin: 0;
`;
root.render(
  <StrictMode>
    <Global>
      <App />
    </Global>
  </StrictMode>
);
